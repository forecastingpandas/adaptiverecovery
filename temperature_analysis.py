import sys
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
import itertools
import numpy as np
import time
from statsmodels.tsa.arima_model import ARIMA
from pandas import Series, DataFrame, Panel
from statsmodels.tsa.stattools import adfuller
sys.path.insert(0, './helper')
from gapService import recognizeGaps, setGapForInterval
from measureService import MAPE
from algorithm import fillGaps, simulateGaps

# ======================================================================================
# Settings
LookBackHoizon = 1000
Gaps = [500,1500,5000]
GapStart = 14000

pdq = (0, 1, 1)
Seasonal_pdq = (0,1,1,12)
TestForStationarity = False
# ======================================================================================

# ======================================================================================
# Read Data from File

n = 31
x = 8
s = 1
i = 4
b = 0
while i <= x:
    while s <= n:
        file = "data/temperature/ARC-2008-"+str(format(i, '02d'))+"-"+str(format(s, '02d'))+".txt"

        print(file)
        try:
            if b == 0:
                oriDataframe = pd.read_csv(file, usecols=[0,1], header=0, sep='\t', names=['Timestamp', 'Value'],  skiprows=[1])
                b=1
            else:
                oriDataframe = oriDataframe.append(pd.read_csv(file, usecols=[0,1], header=0, sep='\t', names=['Timestamp', 'Value'],  skiprows=[1]))
        
        except FileNotFoundError:
            print("File not found!")

        s = s+1
    i = i+1
    s=1



test = pd.to_datetime(oriDataframe.Timestamp, format='%Y%m%d %H:%M', errors='ignore')


oriDataframe['Timestamp'] = test
print(len(oriDataframe))
print(oriDataframe)
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='5T')
periodInSeconds = 300
# ======================================================================================

# ======================================================================================
# Simulate Gaps

simulateGaps(oriDataframe,periodInSeconds,GapStart,Gaps,LookBackHoizon,
            pdq,Seasonal_pdq,TestForStationarity)
# ======================================================================================