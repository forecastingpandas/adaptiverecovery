import rpy2.robjects as ro
from rpy2.robjects import pandas2ri
import pandas as pd
import matplotlib.pyplot as plt
pandas2ri.activate()

ro.r('library(forecast)')

#start
oriDataframe = pd.read_csv('/home/michael/Dokumente/EEDS/adaptiverecovery/btc-market-price.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='D')

rdf = pandas2ri.py2ri(oriDataframe)
ro.globalenv['r_timeseries'] = rdf

print(rdf)
#pred = ro.r('as.data.frame(forecast(auto.arima(r_timeseries),h=5))')

pred = ro.r('auto.arima(r_timeseries,stationary=T)')
ro.globalenv['fit'] = pred
forecast = ro.r('forecast(fit,h=500)')


fig, ax = plt.subplots()
print(oriDataframe)
print(forecast[0][0])
#ax.plot(oriDataframe, 'k--', color='green')
#ax.plot(dataframe, color='black')

new = forecast[0].set_index('Timestamp')
new = forecast[0].asfreq(freq='D')


#d = {'Timestamp' : pd.Series(forecast[0](1)), index=[forecast[0](0)]}
#df = pd.DataFrame(d)
#ax.plot(d, color='red')


plt.show()
