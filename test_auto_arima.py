import sys
import math
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
import itertools
import numpy as np
import pyramid
from pyramid.arima import auto_arima
from bokeh.plotting import figure, show, output_notebook

print('numpy version: %r' % np.__version__)
print('pyramid version: %r' % pyramid.__version__)

from statsmodels.tsa.arima_model import ARIMA
from pandas import Series, DataFrame, Panel

# init bokeh
output_notebook()

def plot_arima(truth, forecasts, title="ARIMA", xaxis_label='Time',
               yaxis_label='Value', c1='#A6CEE3', c2='#B2DF8A', 
               forecast_start=None, **kwargs):
    
    # make truth and forecasts into pandas series
    n_truth = truth
    n_forecasts = forecasts
    
    # always plot truth the same
    truth = pd.Series(truth, index=np.arange(n_truth))
    
    # if no defined forecast start, start at the end
    if forecast_start is None:
        idx = np.arange(n_truth, n_truth + n_forecasts)
    else:
        idx = np.arange(forecast_start, n_forecasts)
    forecasts = pd.Series(forecasts, index=idx)
    
    # set up the plot
    p = figure(title=title, plot_height=400, **kwargs)
    p.grid.grid_line_alpha=0.3
    p.xaxis.axis_label = xaxis_label
    p.yaxis.axis_label = yaxis_label
    
    # add the lines
    p.line(truth.index, truth.values, color=c1, legend='Observed')
    p.line(forecasts.index, forecasts.values, color=c2, legend='Forecasted')
    
    return p

def recognizeGaps(df, freq):

    nan = df[df["Value"].isnull()]
    iterNan = nan.iterrows()
    
    flag = False
    last = []
    gaps = []

    for idx, row in iterNan:
        before  = df.iloc[max(0, df.index.get_loc(idx)-1)]
        after   = df.iloc[min(df.index.get_loc(idx)+1, len(df)-1)]
        nextNan = nan.iloc[min(nan.index.get_loc(idx)+1, len(nan)-1)]

        diff = (nextNan.name - idx).days

        #print(idx)
        #print("diff " + str(diff))

        #cornercase first value (single)
        if before.name == idx and math.isnan(after[0]) == False:
            gaps.append([idx, idx])
            continue

        #cornercase last value (single)
        if after.name == idx and math.isnan(before[0]) == False:
            gaps.append([idx, idx])
            continue

        if diff == 1 and flag == False:
            #more than one record is missing
            #start index
            last = idx
            flag = True
        elif (diff > 1 and math.isnan(before[0])) or (diff == 0 and math.isnan(before[0])) :
            #more than one record is missing
            #end index
            gaps.append([last, idx])
            flag = False
        elif math.isnan(after[0]) == False and math.isnan(before[0]) == False:
            #single record is missing
            gaps.append([idx, idx])
    return gaps
def objfunc(order, exog, endog):
    from statsmodels.tsa.arima_model import ARIMA
    fit = ARIMA(endog, order).fit()
    return fit.aic()
"""
def modelSelection(df):
    y = df.values
    print(y)
    y_train = y[:200]
    y_test  = y[201:]

    p = d = q = range(0, 3)
    pdq = list(itertools.product(p, d, q))
    seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

    best_aic = np.inf
    best_pdq = None
    best_seasonal_pdq = None
    tmp_model = None
    best_mdl = None

    for param in pdq:
        for param_seasonal in seasonal_pdq:
            try:
                tmp_mdl = sm.tsa.statespace.SARIMAX(y_train,
                                                    order = param,
                                                    seasonal_order = param_seasonal,
                                                    enforce_stationarity=True,
                                                    enforce_invertibility=True)
                res = tmp_mdl.fit()
                if res.aic < best_aic:
                    best_aic = res.aic
                    best_pdq = param
                    best_seasonal_pdq = param_seasonal
                    best_mdl = tmp_mdl
            except:
                print("Unexpected error:", sys.exc_info()[0])
                continue
    print("Best SARIMAX{}x{}12 model - AIC:{}".format(best_pdq, best_seasonal_pdq, best_aic))
"""

#start
oriDataframe = pd.read_csv('/home/michael/Dokumente/EEDS/adaptiverecovery/btc-market-price.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='D')

dataframe = pd.read_csv('/home/michael/Dokumente/EEDS/adaptiverecovery/btc-market-price-2_gap.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
dataframe = dataframe.set_index('Timestamp')
dataframe = dataframe.asfreq(freq='D')

#print(dataframe.index.freq)
#print(dataframe.isfinite[1])

#check if dataset is complete
if (dataframe.isnull().sum().Value > 0):
    print("dataset is not complete")
    gaps = recognizeGaps(dataframe, "days")

    plt.plot(oriDataframe, color='green')
    plt.plot(dataframe, color='black')
    i=0



    for gap in gaps:
        print(str(i)+" -> "+str(gap))

        startIdx = dataframe.index.get_loc(gap[0])
        endIdx   = dataframe.index.get_loc(gap[1])
        print("start " + str(startIdx) + " end " + str(endIdx))
        
        prevEndIdx = 0
        if(i>0):
           prevEndIdx = dataframe.index.get_loc(gaps[i-1][1])
        print("prevEndIdx: "+str(prevEndIdx))

        testDataHoizon = max(prevEndIdx,startIdx-200)
        print("Horizon: "+str(testDataHoizon))

        lastgap = testDataHoizon
        if(i>0):
            lastgap = dataframe.index.get_loc(gaps[i-1][1])+2

        from scipy.optimize import brute
        grid = (slice(1, 3, 1), slice(1, 3, 1), slice(1, 3, 1))
        brute(objfunc, grid, args=(dataframe[testDataHoizon:startIdx], ''), finish=None)

        model = auto_arima(dataframe[lastgap:startIdx], start_p=1, start_q=1, max_p=10, max_q=3, m=12,
                    start_P=0, seasonal=True, n_jobs=-1, d=0, D=0, trace=True,
                    error_action='ignore',  # don't want to know if an order does not work
                    suppress_warnings=True,  # don't want convergence warnings
                    stepwise=True,random=True, random_state=42,  # we can fit a random search (not exhaustive)
                    n_fits=(endIdx-startIdx)+2)
        #print(model)
        #modelFit = model.fit(disp=0)
        #print(modelFit.summary())
        forecast = model.predict(n_periods=(endIdx-startIdx)+2)

        base = gap[1]
        date_list = [base - datetime.timedelta(days=x) for x in range(0, (endIdx-startIdx)+2)]

        print(date_list)



        df = pd.DataFrame({'Timestamp':np.array(date_list), 'Value': np.array(forecast)})
        df = df.set_index('Timestamp')
        print(df)

        plt.plot(df, color='red')
        i+=1    

    plt.show()    
    
else:
    print("dataset is complete")
    
