import sys
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm

from statsmodels.tsa.arima_model import ARIMA
from pandas import Series, DataFrame, Panel

def recognizeGaps(df, freq):

    nan = df[df["Value"].isnull()]
    iterNan = nan.iterrows()
    
    flag = False
    last = []
    gaps = []

    for idx, row in iterNan:
        before  = df.iloc[max(0, df.index.get_loc(idx)-1)]
        after   = df.iloc[min(df.index.get_loc(idx)+1, len(df)-1)]
        nextNan = nan.iloc[min(nan.index.get_loc(idx)+1, len(nan)-1)]

        diff = (nextNan.name - idx).days

        #print(idx)
        #print("diff " + str(diff))

        #cornercase first value (single)
        if before.name == idx and math.isnan(after[0]) == False:
            gaps.append([idx, idx])
            continue

        #cornercase last value (single)
        if after.name == idx and math.isnan(before[0]) == False:
            gaps.append([idx, idx])
            continue

        if diff == 1 and flag == False:
            #more than one record is missing
            #start index
            last = idx
            flag = True
        elif (diff > 1 and math.isnan(before[0])) or (diff == 0 and math.isnan(before[0])) :
            #more than one record is missing
            #end index
            gaps.append([last, idx])
            flag = False
        elif math.isnan(after[0]) == False and math.isnan(before[0]) == False:
            #single record is missing
            gaps.append([idx, idx])
    return gaps

#start
oriDataframe = pd.read_csv('C:\\Users\\Mango\\Desktop\\Uni\\eeds\\adaptiverecovery\\btc-market-price.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='D')

dataframe = pd.read_csv('C:\\Users\\Mango\\Desktop\\Uni\\eeds\\adaptiverecovery\\btc-market-price-gap.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
dataframe = dataframe.set_index('Timestamp')
dataframe = dataframe.asfreq(freq='D')

#print(dataframe.index.freq)
#print(dataframe.isfinite[1])

#check if dataset is complete
if (dataframe.isnull().sum().Value > 0):
    print("dataset is not complete")
    gaps = recognizeGaps(dataframe, "days")

    for i in gaps:
        print(i)

    startIdx = dataframe.index.get_loc(gaps[0][0])
    endIdx   = dataframe.index.get_loc(gaps[0][1])
    print("start " + str(startIdx) + " end " + str(endIdx))
    
    fit1 = sm.tsa.statespace.SARIMAX(dataframe.Value[0:startIdx], order=(2, 1, 4),seasonal_order=(0,1,1,7)).fit()
    y_hat = fit1.predict(start=startIdx-1, end=endIdx+1, dynamic=True)
    
    plt.plot(oriDataframe, color='green')
    plt.plot(dataframe, color='black')
    plt.plot(y_hat, color='red')
    plt.show()    
    
    #model = ARIMA(dataframe[0:startIdx], order=(7,0,1))
    #print(model)
    #modelFit = model.fit(disp=0)
    #print(modelFit.summary())
    #forecast = modelFit.predict(start=startIdx-1, end=endIdx+1)
    #plt.plot(oriDataframe, color='green')
    #plt.plot(dataframe, color='black')
    #plt.plot(forecast, color='red')
    #plt.show()    
    
else:
    print("dataset is complete")
    
