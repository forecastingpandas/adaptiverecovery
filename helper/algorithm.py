import sys
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
import itertools
import numpy as np
import time
import datetime
import rpy2.robjects as ro
from rpy2.robjects import pandas2ri
from statsmodels.tsa.arima_model import ARIMA
from pandas import Series, DataFrame, Panel
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa import holtwinters
sys.path.insert(0, '.')
from gapService import recognizeGaps, setGapForInterval
from measureService import MAPE
from statsmodels.tsa.api import ExponentialSmoothing

# ======================================================================================
# Prepare Data with Gaps

def simulateGaps(oriDataframe,periodInSeconds,GapStart,Gaps,LookBackHoizon,pdq,Seasonal_pdq,TestForStationarity):
    resultSet = []
    for gap in Gaps:
        dataframe = oriDataframe.copy()
        setGapForInterval(dataframe,GapStart,GapStart+gap)

        #plt.plot(oriDataframe, 'k--', color='green')
        #plt.plot(dataframe, color='black')
        #plt.show()

        gapSimulationResult = fillGaps(oriDataframe, dataframe, periodInSeconds, LookBackHoizon,pdq,Seasonal_pdq,TestForStationarity)
        print(gapSimulationResult)
        resultSet.append(gapSimulationResult)
    printResultSet(resultSet,GapStart,Gaps)    
# ======================================================================================

def printResultSet(resultSet,GapStart,Gaps):
    print("\n\n======================================================================")
    print("= ResultSet ==========================================================")
    i = 0
    for result in resultSet:
        print("======================================================================")
        print("Gap start: %d" % (GapStart))
        print("Gap length: %d" % (Gaps[i]))
        print("\n- ARIMA -----------------------")
        print("runs: %f sec" % (result[0][0][0]))
        print("MAPE: %f" % result[0][0][1])
        print("\n- Holt Winters ----------------")
        print("runs: %f sec" % result[0][1][0])
        print("MAPE: %f" % result[0][1][1])
        print("\n- ETS -------------------------")
        print("runs: %f sec" % result[0][2][0])
        print("MAPE: %f" % result[0][2][1])
        #print("\n")
        i += 1

# ======================================================================================

def fillGaps(oriDataframe, dataframe, periodInSeconds, LookBackHoizon,pdq,Seasonal_pdq,TestForStationarity):
    result = []
    # ======================================================================================
    # Analyze the given Dataset for completeness
    if (dataframe.isnull().sum().Value > 0):
        print("dataset is not complete")
        gaps = recognizeGaps(dataframe,periodInSeconds)

        #Print Original data series + the Gap dataseries
        fig, ax = plt.subplots()
        ax.plot(oriDataframe, 'k--', color='green')
        ax.plot(dataframe, color='black')

        # ======================================================================================
        i = 0
        for gap in gaps:
            gapResult = []
            # ===============================================
            # Forecast each Gap

            #Get Index of the Gap From/To
            startIdx = dataframe.index.get_loc(gap[0])
            endIdx   = dataframe.index.get_loc(gap[1])
            
            # ===============================================
            # Calculate the lookback horizon
            prevEndIdx = 0
            if(i>0):
                prevEndIdx = dataframe.index.get_loc(gaps[i-1][1])
            testDataHoizon = max(prevEndIdx,startIdx-LookBackHoizon)

            # ===============================================
            # Model Configuration
            best_pdq = pdq
            stationarity = False
            best_seasonal_pdq = Seasonal_pdq

            # ===============================================
            # Augmented Dickey-Fuller test
            if(TestForStationarity):
                dataF = dataframe[testDataHoizon:startIdx]
                adfResult = adfuller(dataF['Value'].values)
                print('ADF Statistic: %f' % adfResult[0])
                print('p-value: %f' % adfResult[1])
                
                if(adfResult[1]<=0.05):
                    best_pdq = (0,0,0)
                    stationarity = True

            # ===============================================
            # ARIMA - Get Model on the training Data
            arimaStart = time.time()
            modelFit = sm.tsa.statespace.SARIMAX(dataframe[testDataHoizon:startIdx], order=best_pdq,seasonal_order=best_seasonal_pdq, enforce_stationarity=stationarity).fit()
            #Predict the Model for the Gap interval
            pred = modelFit.get_prediction(start=startIdx-1-testDataHoizon, end=endIdx+1-testDataHoizon,dynamic=True)
            arimaEnd = time.time()
            pred_ci = pred.conf_int()

            # ===============================================
            # Calculate MAPE (Mean Absolute Percentage Error)
            mapeARIMA = MAPE(pred.predicted_mean,oriDataframe[startIdx-1:endIdx+2].values)
            
            # Result for ARIMA
            gapResult.append([arimaEnd-arimaStart,mapeARIMA])
            

            # ===============================================
            # Holt Winters - Get Model on the training Data
            gapsize = endIdx-startIdx
            holtStart = time.time()
            holtModel = holtwinters.ExponentialSmoothing(np.asarray(dataframe[testDataHoizon:startIdx]) ,seasonal_periods=((int)(gapsize/2)), trend='add', seasonal=None).fit()
            holtforecast = holtModel.predict(start=startIdx-1-testDataHoizon, end=endIdx+1-testDataHoizon)
            holtEnd = time.time()
            date_list = [gap[1] - datetime.timedelta(seconds= (x*periodInSeconds)) for x in range(-1, ((endIdx+1-testDataHoizon)-(startIdx-1-testDataHoizon)))]

            dfh = pd.DataFrame({'Timestamp':np.array(date_list), 'Value': np.array(holtforecast)});
            dfh = dfh.set_index('Timestamp')
            #dfh = dfh.asfreq(freq='D')
            mapeHolt = MAPE(dfh.values, oriDataframe[startIdx-1:endIdx+2].values)

            # Result for Holt Winters
            gapResult.append([holtEnd-holtStart,mapeHolt])

            # ===============================================
            # ETS - Get Model on the training Data
            ro.r('library(forecast)')
            rdf = pandas2ri.py2ri(dataframe[testDataHoizon:startIdx])
            #R call
            ro.globalenv['r_timeseries'] = rdf
            start=startIdx-1-testDataHoizon
            end=endIdx+2-testDataHoizon
            interval = end - start
            #print('Interval: ')
            #print(interval) 
            #R call
            ro.globalenv['interval'] = interval
            ro.globalenv['stat'] = 'T'
            #R calls
            etsStart = time.time()
            predETS = ro.r('ets(ts(r_timeseries))')
            ro.globalenv['fit'] = predETS
            forecast = (ro.r('as.data.frame(forecast(fit,h=interval,stationary=stat))'))
            etsEnd = time.time()
            
            dfEts = pd.DataFrame({'Timestamp':np.array(date_list), 'Value': np.array(forecast[0])});
            dfEts = dfEts.set_index('Timestamp')

            mapeETS = MAPE(forecast[0], oriDataframe[startIdx-1:endIdx+2].values)

            # Result for ETS
            gapResult.append([etsEnd-etsStart,mapeETS])

            # ===============================================
            # Plot the forecast
            plotLableArima = 'ARIMA:   {:.2f}s - MAPE: {:.2f}'.format(arimaEnd-arimaStart,mapeARIMA)
            plotLableHolt =  'Holt W.:  {:.2f}s - MAPE: {:.2f}'.format(holtEnd-holtStart,mapeHolt)
            plotLableEts =   'ETS:       {:.2f}s - MAPE: {:.2f}'.format(etsEnd-etsStart,mapeETS)

            ax.plot(pred.predicted_mean,label=plotLableArima, color='red')
            ax.fill_between(pred_ci.index, 
                    pred_ci.iloc[:, 0], 
                    pred_ci.iloc[:, 1], color='#ff0066', alpha=.25);

            ax.plot(dfh, label=plotLableHolt, color="blue");
            #ax.fill_between(dfh.index, 
            #        dfh.iloc[:, 0], 
            #        dfh.iloc[:, 1], color='#ADD8E6', alpha=.25);
            
            ax.plot(dfEts, label=plotLableEts, color="orange");

            result.append(gapResult)
            i+=1
        # ======================================================================================

        # Prepare the Plot with Legend
        legend = ax.legend(loc='upper left', shadow=True)
        frame = legend.get_frame()
        frame.set_facecolor('0.90')
        for label in legend.get_texts():
            label.set_fontsize('large')
        for label in legend.get_lines():
            label.set_linewidth(1.5)
        plt.show()    
        # ======================================================================================

    # ======================================================================================
    print("dataset is complete")

    return result
# ======================================================================================