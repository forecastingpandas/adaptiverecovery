import math

def setGapForInterval(df,fromIdx,toIdx):
    for idx in range(fromIdx,toIdx):
        df.set_value(df.index[idx], 'Value', float('nan'))
    return df

def recognizeGaps(df,frequencyInSeconds):

    nan = df[df["Value"].isnull()]
    iterNan = nan.iterrows()
    
    flag = False
    last = []
    gaps = []

    for idx, row in iterNan:
        before  = df.iloc[max(0, df.index.get_loc(idx)-1)]
        after   = df.iloc[min(df.index.get_loc(idx)+1, len(df)-1)]
        nextNan = nan.iloc[min(nan.index.get_loc(idx)+1, len(nan)-1)]

        diff = (nextNan.name - idx).total_seconds()

        #print(idx)
        #print("diff " + str(diff))

        #cornercase first value (single)
        if before.name == idx and math.isnan(after[0]) == False:
            gaps.append([idx, idx])
            continue

        #cornercase last value (single)
        if after.name == idx and math.isnan(before[0]) == False:
            gaps.append([idx, idx])
            continue

        if diff == frequencyInSeconds and flag == False:
            #more than one record is missing
            #start index
            last = idx
            flag = True
        elif (diff > frequencyInSeconds and math.isnan(before[0])) or (diff == 0 and math.isnan(before[0])) :
            #more than one record is missing
            #end index
            gaps.append([last, idx])
            flag = False
        elif math.isnan(after[0]) == False and math.isnan(before[0]) == False:
            #single record is missing
            gaps.append([idx, idx])
    return gaps