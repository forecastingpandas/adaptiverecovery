import sys
import math
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#import statsmodels.api as sm
import itertools
import numpy as np


#from statsmodels.tsa.arima_model import ARIMA
from pandas import Series, DataFrame, Panel
from statsmodels.tsa.api import ExponentialSmoothing
#from statsmodels.tsa.api import ExponentialSmoothing

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def recognizeGaps(df, freq):

    nan = df[df["Value"].isnull()]
    iterNan = nan.iterrows()
    
    flag = False
    last = []
    gaps = []

    for idx, row in iterNan:
        before  = df.iloc[max(0, df.index.get_loc(idx)-1)]
        after   = df.iloc[min(df.index.get_loc(idx)+1, len(df)-1)]
        nextNan = nan.iloc[min(nan.index.get_loc(idx)+1, len(nan)-1)]

        diff = (nextNan.name - idx).days

        #print(idx)
        #print("diff " + str(diff))

        #cornercase first value (single)
        if before.name == idx and math.isnan(after[0]) == False:
            gaps.append([idx, idx])
            continue

        #cornercase last value (single)
        if after.name == idx and math.isnan(before[0]) == False:
            gaps.append([idx, idx])
            continue

        if diff == 1 and flag == False:
            #more than one record is missing
            #start index
            last = idx
            flag = True
        elif (diff > 1 and math.isnan(before[0])) or (diff == 0 and math.isnan(before[0])) :
            #more than one record is missing
            #end index
            gaps.append([last, idx])
            flag = False
        elif math.isnan(after[0]) == False and math.isnan(before[0]) == False:
            #single record is missing
            gaps.append([idx, idx])
    return gaps

"""
def modelSelection(df):
    y = df.values
    print(y)
    y_train = y[:200]
    y_test  = y[201:]

    p = d = q = range(0, 3)
    pdq = list(itertools.product(p, d, q))
    seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

    best_aic = np.inf
    best_pdq = None
    best_seasonal_pdq = None
    tmp_model = None
    best_mdl = None

    for param in pdq:
        for param_seasonal in seasonal_pdq:
            try:
                tmp_mdl = sm.tsa.statespace.SARIMAX(y_train,
                                                    order = param,
                                                    seasonal_order = param_seasonal,
                                                    enforce_stationarity=True,
                                                    enforce_invertibility=True)
                res = tmp_mdl.fit()
                if res.aic < best_aic:
                    best_aic = res.aic
                    best_pdq = param
                    best_seasonal_pdq = param_seasonal
                    best_mdl = tmp_mdl
            except:
                print("Unexpected error:", sys.exc_info()[0])
                continue
    print("Best SARIMAX{}x{}12 model - AIC:{}".format(best_pdq, best_seasonal_pdq, best_aic))
"""

#start
oriDataframe = pd.read_csv('btc-market-price.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='D')

dataframe = pd.read_csv('btc-market-price-2_gap.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
dataframe = dataframe.set_index('Timestamp')
dataframe = dataframe.asfreq(freq='D')

#print(dataframe.index.freq)
#print(dataframe.isfinite[1])

#check if dataset is complete
if (dataframe.isnull().sum().Value > 0):
    print("dataset is not complete")
    gaps = recognizeGaps(dataframe, "days")

    fig, ax = plt.subplots()

    ax.plot(oriDataframe, 'k--', color='green')
    ax.plot(dataframe, color='black')

    i = 0
    for gap in gaps:
        print(str(i)+" -> "+str(gap))

        startIdx = dataframe.index.get_loc(gap[0])
        endIdx   = dataframe.index.get_loc(gap[1])
        print("start " + str(startIdx) + " end " + str(endIdx))
        
        prevEndIdx = 0
        if(i>0):
           prevEndIdx = dataframe.index.get_loc(gaps[i-1][1])
        print("prevEndIdx: "+str(prevEndIdx))

        testDataHoizon = max(prevEndIdx,startIdx-100)
        print("Horizon: "+str(testDataHoizon))

        
        fit1 = ExponentialSmoothing(np.asarray(dataframe[testDataHoizon:startIdx]) ,seasonal_periods=7 ,trend='add', seasonal='add',).fit()
        forecast = fit1.forecast((endIdx-startIdx)+2)
        print(forecast)

        base = gap[1]
        date_list = [base - datetime.timedelta(days=x) for x in range(0, (endIdx-startIdx)+2)]

        print(date_list)

        df = pd.DataFrame({'Timestamp':np.array(date_list), 'Value': np.array(forecast)})
        df = df.set_index('Timestamp')
        #plt.plot(forecast, color="red")
        
        """
        y_hat_avg['Holt_Winter'] = fit1.forecast(len(test))
        plt.figure(figsize=(16,8))
        plt.plot( train['Count'], label='Train')
        plt.plot(test['Count'], label='Test')
        plt.plot(y_hat_avg['Holt_Winter'], label='Holt_Winter')
        plt.legend(loc='best')
        plt.show()
        """
        
        """
        model = ARIMA(dataframe[testDataHoizon:startIdx], order=(7,0,1))
        #print(model)
        modelFit = model.fit(disp=0)
        #print(modelFit.summary())
        forecast = modelFit.predict(start=startIdx-1-testDataHoizon, end=endIdx+1-testDataHoizon)
        """

        
        print("=== Forecast ======================")
        #print(forecast.values)
        print("===================================")
        print("=== OrigData ======================")
        #print(oriDataframe[startIdx-1:endIdx+2].values)
        print("===================================")
        

        #mape = mean_absolute_percentage_error(forecast.values,oriDataframe[startIdx-1:endIdx+2].values)
        #print(mape)

        plotLable = 'Forecast '+str(i+1)+' - MAPE: '#+str(mape)
        ax.plot(df, color='red', label=plotLable)
        i+=1
    

    # Now add the legend with some customizations.
    legend = ax.legend(loc='upper center', shadow=True)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width
    plt.show()    
    

    print("dataset is complete")
    
