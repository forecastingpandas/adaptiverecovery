# EEDS Project SS2018

## Teammembers

Michael Eder         - 1325852  
Lukas Leitzinger    - 1327140  
Benjamin K�rbel    - 1228896  

## Installation

We recommend to use anaconda (https://anaconda.org/) to install the needed python dependencies/libraries.

## Run the scripts

python heartrate_analysis.py  
python bitcoin_analysis.py  
python heartrate_analysis.py  

## Project Information

We have implemented our simulation algorithm in Python. Furthermore we used the Python data analysis library pandas. In a later phase of the project, we added rpy2 to cover forecast methods, which are not supported by pandas. 

As forecasting methods, we selected ETS, Arima and  Holt-Winters. All three aforementioned forecasting methods were applied on three different datasets. Thus, the datasets are complete, we created three gaps of different lengths for each dataset artificially. Due to better comparability, the start position of each gap per dataset is fixed (respectively, the position can be changed per program execution). 

We have chosen the following datasets:
heart-rate chart: 17000 data points, frequency: 0.5 seconds
bitcoin-chart: 730 data points, frequency: daily
temperature-chart: 40000 data points, frequency: 5 minutes

The report is structured as follows. In the next chapter, our decisions, for example regarding the implementation choices, over the entire project (divided into three phases) progress are described. The second chapter declares the structure of our algorithm. In order, the analysis of the datasets including a comparison by the means of appropriate classification numbers is conducted. The last chapter contains a conclusion of the lab and suggestions for further improvements. 


