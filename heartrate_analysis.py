import sys
import pandas as pd
sys.path.insert(0, './helper')
from algorithm import simulateGaps

# ======================================================================================
# Settings
LookBackHoizon = 1000
Gaps = [500,1500,5000]
GapStart = 4000

pdq = (2, 1, 4)
Seasonal_pdq = (0,1,1,7)
TestForStationarity = True
# ======================================================================================

# ======================================================================================
# Read Data from File
oriDataframe = pd.read_csv('data/h.dat', names=['Value'])
timestamps = pd.date_range(start='2018-01-01', periods=len(oriDataframe.index), freq='500L')
oriDataframe['Timestamp'] = timestamps
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='500L')
periodInSeconds = 0.5
# ======================================================================================

# ======================================================================================
# Simulate Gaps
simulateGaps(oriDataframe,periodInSeconds,GapStart,Gaps,LookBackHoizon,
            pdq,Seasonal_pdq,TestForStationarity)
# ======================================================================================