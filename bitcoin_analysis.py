import sys
import pandas as pd
sys.path.insert(0, './helper')
from algorithm import simulateGaps

# ======================================================================================
# Settings
LookBackHoizon = 80
Gaps = [50,200,300]
GapStart = 370

pdq = (2, 1, 4)
Seasonal_pdq = (0,1,1,7)
TestForStationarity = True
# ======================================================================================

# ======================================================================================
# Read Data from File
oriDataframe = pd.read_csv('data/btc-market-price.csv', names=['Timestamp', 'Value'], parse_dates=['Timestamp'])
oriDataframe = oriDataframe.set_index('Timestamp')
oriDataframe = oriDataframe.asfreq(freq='D')
periodInSeconds = 60*60*24
# ======================================================================================

# ======================================================================================
# Simulate Gaps
simulateGaps(oriDataframe,periodInSeconds,GapStart,Gaps,LookBackHoizon,
            pdq,Seasonal_pdq,TestForStationarity)
# ======================================================================================